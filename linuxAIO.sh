#!/bin/sh
echo ""
echo "Welcome to NadekoBot. Downloading the latest installer..."
branch="https://gitlab.com/blackskye/NadekoBot-BashScript/raw/nadek0"
root=$(pwd)
wget -N $branch/nadeko_master_installer.sh

bash nadeko_master_installer.sh
cd "$root"
rm "$root/nadeko_master_installer.sh"
exit 0